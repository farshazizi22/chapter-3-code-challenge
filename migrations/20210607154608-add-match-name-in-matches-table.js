"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        queryInterface.addColumn("matches", "match_name", {
            type: Sequelize.STRING,
        });
    },

    down: async (queryInterface, Sequelize) => {
        queryInterface.removeColumn("matches", "match_name");
    },
};
