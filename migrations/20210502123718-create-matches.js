"use strict";
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("matches", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            player_1: {
                type: Sequelize.INTEGER,
                references: {
                    model: "biodatas",
                    key: "id",
                    onUpdate: "cascade",
                    onDelete: "cascade",
                },
            },
            player_1_hand: {
                type: Sequelize.STRING,
            },
            player_2: {
                type: Sequelize.INTEGER,
                references: {
                    model: "biodatas",
                    key: "id",
                    onUpdate: "cascade",
                    onDelete: "cascade",
                },
            },
            player_2_hand: {
                type: Sequelize.STRING,
            },
            result: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("matches");
    },
};
