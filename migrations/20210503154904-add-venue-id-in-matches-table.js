"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        queryInterface.addColumn("matches", "venue_id", {
            type: Sequelize.INTEGER,
            references: {
                model: "venues",
                key: "id",
                onUpdate: "cascade",
                onDelete: "cascade",
            },
        });
    },

    down: async (queryInterface, Sequelize) => {
        queryInterface.removeColumn("matches", "venue_id");
    },
};
