module.exports = (req, res, next) => {
    if(req.isAuthenticated()) {
        if (req.user.role == "Admin") {
            return next();
        }
    }
    res.redirect("/user/login");
}
