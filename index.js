const express = require("express");
const app = express();
const port = 8000;

// const biodataRouter = require("./routes/biodata");
// const matchRouter = require("./routes/match");
// const userRouter = require("./routes/user");
// const venueRouter = require("./routes/venue");
const router = require("./routes");

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.urlencoded({ extended: false }));

// app.use("/biodata", biodataRouter);
// app.use("/match", matchRouter);
// app.use("/user", userRouter);
// app.use("/venue", venueRouter);
app.use(router);

app.get("/", (req, res) => {
    res.render("index");
});

app.get("/dashboard", (req, res) => {
    res.render("dashboard");
});

app.get("/games", (req, res) => {
    res.render("games");
});

app.listen(port);