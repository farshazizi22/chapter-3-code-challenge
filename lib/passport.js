const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const { users } = require("../models");

async function authenticate(username, password, done) {
    try {
        const user = await users.authenticate({ username, password });
        return done(null, user); // line ini belum disimpan ke session
    } catch (error) {
        return done(null, false, { message: error.message });
    }
}

passport.use(
    new localStrategy(
        { usernameField: "username", passwordField: "password" },
        authenticate
    )
);

// serializeUser: digunakan untuk simpan ke session dan hanya id saja yang disimpan
passport.serializeUser(function (user, done) {
    return done(null, user.id);
});

// deserializeUser: ketika kita butuh data user
passport.deserializeUser(async function (id, done) {
    return done(null, await users.findByPk(id));
});

module.exports = passport;

/* 
catatan:
1. done(null, user); 
    parameter null pada function done adalah error dan masih belum tahu apakah bisa dikirim error biasa atau tidak 
*/
