var express = require("express");
const restrict = require("../middlewares/restrict");
var router = express.Router();

/* GET home page. */
router.get("/", (req, res) => {
    res.render("index");
});

router.get("/dashboard", restrict, (req, res) => {
    res.render("dashboard");
});

router.get("/games", (req, res) => {
    res.render("games");
});

module.exports = router;
