const router = require("express").Router();
const userController = require("../controllers/userController");
const restrict = require("../middlewares/restrict");

router.get("/login", userController.loginUser);
// router.post("/login/save", userController.loginSaveUser);
router.post("/login/save", userController.loginAction);
router.get("/sign-up", userController.signUpUser);
router.post("/sign-up/save", userController.signUpSaveUser);

module.exports = router;