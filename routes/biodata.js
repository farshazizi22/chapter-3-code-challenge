const router = require("express").Router();
const biodataController = require("../controllers/biodataController");

router.get("/list", biodataController.listBiodata);
router.get("/create", biodataController.createBiodata);
router.post("/save", biodataController.saveBiodata);
router.get("/detail/:id", biodataController.detailBiodata);
router.get("/edit/:id", biodataController.editBiodata);
router.post("/update", biodataController.updateBiodata);
router.get("/destroy/:id", biodataController.destroyBiodata);

module.exports = router;