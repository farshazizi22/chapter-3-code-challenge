const router = require("express").Router();
const matchController = require("../controllers/matchController");

router.get("/list", matchController.listMatch);
router.get("/create", matchController.createMatch);
router.post("/save", matchController.saveMatch);
router.get("/detail/:id", matchController.detailMatch);
// router.get("/edit/:id", matchController.editMatch);
// router.post("/update", matchController.updateMatch);
// router.get("/destroy/:id", matchController.destroymatch);

module.exports = router;
