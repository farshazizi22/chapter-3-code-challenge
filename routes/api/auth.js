var express = require("express");
var router = express.Router();
const userController = require("../../controllers/api/userController");
const restrictJwt = require("../../middlewares/restrict-jwt");

router.post("/register", userController.registerAction);
router.post("/login", userController.loginAction);
// router.get("/profile", restrictJwt, userController.profileAction);

module.exports = router;
