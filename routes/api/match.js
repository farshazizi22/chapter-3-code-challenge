var express = require("express");
var router = express.Router();
const matchController = require("../../controllers/api/matchController");
const restrictJwt = require("../../middlewares/restrict-jwt");

router.post("/create-match", restrictJwt, matchController.createMatchAction);
router.post("/fight/:match_id", restrictJwt, matchController.fightAction);
router.get("/detail-match/:id", restrictJwt, matchController.detailMatchAction);

module.exports = router;
