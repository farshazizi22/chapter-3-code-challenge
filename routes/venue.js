const router = require("express").Router();
const venueController = require("../controllers/venueController");

router.get("/list", venueController.listVenue);
router.get("/create", venueController.createVenue);
router.post("/save", venueController.saveVenue);
router.get("/detail/:id", venueController.detailVenue);
router.get("/edit/:id", venueController.editVenue);
router.post("/update", venueController.updateVenue);
router.get("/destroy/:id", venueController.destroyVenue);

module.exports = router;
