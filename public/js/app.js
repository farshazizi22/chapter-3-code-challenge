class Hand {
    constructor(hand) {
        this.hand = hand;
    }

    getHand() {
        return this.hand;
    }
}

class Player extends Hand {
    constructor(hand) {
        super(hand);
    }
}

class Computer extends Hand {
    constructor() {
        let hand = Math.floor(Math.random() * Math.floor(3));
        if (hand == 0) {
            hand = "rock";
        } else if (hand == 1) {
            hand = "paper";
        } else {
            hand = "scissors";
        }
        super(hand);
    }
}

class Match {
    constructor(playerA, playerB) {
        this.playerA = playerA;
        this.playerB = playerB;
    }

    getResult() {
        // console.log(`playerA: ${this.playerA}`);
        // console.log(`playerB: ${this.playerB}`);
        let result;
        if (this.playerA == this.playerB) {
            result = "DRAW";
        }
        // Player 1 Rock
        if (this.playerA == "rock" && this.playerB == "paper") {
            result = "COM WIN";
        }
        if (this.playerA == "rock" && this.playerB == "scissors") {
            result = "PLAYER 1 WIN";
        }
        // Player 1 Paper
        if (this.playerA == "paper" && this.playerB == "rock") {
            result = "PLAYER 1 WIN";
        }
        if (this.playerA == "paper" && this.playerB == "scissors") {
            result = "COM WIN";
        }
        // Player 1 Scissors
        if (this.playerA == "scissors" && this.playerB == "rock") {
            result = "COM WIN";
        }
        if (this.playerA == "scissors" && this.playerB == "paper") {
            result = "PLAYER 1 WIN";
        }

        return result;
    }
}

let textVS = document.getElementById("text-vs");
let playerHand = document.getElementsByClassName("player-hand");
let computerHand = document.getElementsByClassName("computer-hand");
let btnRefresh = document.getElementById("btn-refresh");
for (let i = 0; i < playerHand.length; i++) {
    let playerChoiceHand = playerHand[i];
    playerChoiceHand.addEventListener("click", function (e) {
        // step 1: reset
        resetPlayerChoiceHand(playerHand);
        resetComputerChoiceHand(computerHand);

        // step 2:
        let player = new Player(playerChoiceHand.id.toLowerCase());
        console.log(`user choice: ${player.hand}`);
        let computer = new Computer();
        console.log(`computer choice: ${computer.getHand()}`);
        let match = new Match(player.hand, computer.getHand());
        console.log(`user choice: ${player.hand}, computer choice: ${computer.getHand()}, result: ${match.getResult()}`);
        let textResult = match.getResult();

        // step 3:
        playerChoiceHand.classList.add("selected");
        let computerChoiceHand = document.getElementById(`computer-${computer.getHand()}`);
        computerChoiceHand.classList.add("selected");

        // step 4:
        setText(textResult);
    });
}

btnRefresh.addEventListener("click", function (e) {
    resetGame();
});

function resetPlayerChoiceHand(hands) {
    for (let i = 0; i < hands.length; i++) {
        let hand = hands[i];
        hand.classList.remove("selected");
    }
}

function resetComputerChoiceHand(hands) {
    for (let i = 0; i < hands.length; i++) {
        let hand = hands[i];
        hand.classList.remove("selected");
    }
}

function setText(textResult) {
    textVS.classList.remove("text-start");
    textVS.classList.add("text-result");
    textVS.innerText = `${textResult}`;
}

function resetText() {
    textVS.classList.add("text-start");
    textVS.classList.remove("text-result");
    textVS.innerText = "V S";
}

function resetGame() {
    resetPlayerChoiceHand(playerHand);
    resetComputerChoiceHand(computerHand);
    resetText();
}