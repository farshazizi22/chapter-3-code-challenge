"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt"); // tambahan
const jwt = require("jsonwebtoken"); // tambahan

module.exports = (sequelize, DataTypes) => {
    class users extends Model {
        static associate(models) {
            models.users.belongsTo(models.biodatas, {
                foreignKey: "biodata_id",
                as: "biodataId",
            });
        }

        // tambahan
        static #encrypt = function (password) {
            // salt key
            // bcrypt.hashSync(password, 10); 10 = yang dimaksud salt key, bisa dimasukkan string juga
            return bcrypt.hashSync(password, 10);
        };

        static register = function ({ username, password, role }) {
            let encryptPassword = this.#encrypt(password);
            return this.create({
                username,
                password: encryptPassword,
                role: role,
            });
        };

        checkPassword = function (password) {
            return bcrypt.compareSync(password, this.password);
        };

        generateToken = function () {
            const payLoad = {
                id: this.id,
                username: this.username,
            };

            const secret = "campur rasa";
            return jwt.sign(payLoad, secret);
        };

        static authenticate = async function ({ username, password }) {
            try {
                const user = await this.findOne({ where: { username } });
                if (!user) {
                    return Promise.reject("User tidak ditemukan!");
                }
                if (!user.checkPassword(password)) {
                    return Promise.reject("Invalid username & password");
                }
                return Promise.resolve(user);
            } catch (error) {
                return Promise.resolve(error);
            }
        };

        static checkUser = async function ({ username, password }) {
            try {
                const user = await this.findOne({ where: { username } });
                if (!user.checkPassword(password)) {
                    return false;
                }
                return user;
            } catch (error) {
                
            }
        };
        // end tambahan
    }
    users.init(
        {
            username: DataTypes.STRING,
            password: DataTypes.STRING,
            role: DataTypes.STRING,
            biodata_id: {
                type: DataTypes.INTEGER,
                field: "biodata_id",
            },
        },
        {
            sequelize,
            modelName: "users",
        }
    );
    return users;
};
