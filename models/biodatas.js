"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class biodatas extends Model {
        static associate(models) {
            models.biodatas.hasMany(models.matches, {
                foreignKey: "id",
            });
        }

        static createBiodata = function ({ full_name, name, dob, gender, address}) {
            return this.create({
                full_name: full_name,
                name: name,
                dob: dob,
                gender: gender,
                address: address,
            });
        };
    }
    biodatas.init(
        {
            full_name: DataTypes.STRING,
            name: DataTypes.STRING,
            dob: DataTypes.DATE,
            gender: DataTypes.INTEGER,
            address: DataTypes.TEXT,
        },
        {
            sequelize,
            modelName: "biodatas",
        }
    );
    return biodatas;
};
