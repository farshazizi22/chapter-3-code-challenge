"use strict";
const { Model } = require("sequelize");
const { users } = require("../models");
module.exports = (sequelize, DataTypes) => {
    class matches extends Model {
        static associate(models) {
            models.matches.belongsTo(models.biodatas, {
                foreignKey: "player_1",
                as: "playerOne",
            });

            models.matches.belongsTo(models.biodatas, {
                foreignKey: "player_2",
                as: "playerTwo",
            });

            models.matches.belongsTo(models.venues, {
                foreignKey: "venue_id",
                as: "venue",
            });
        }

        static createMatch = function ({ match_name }) {
            return this.create({
                match_name: match_name,
            });
        };

        static fight = async function (match_id, { hand }, biodata_id) {
            let dataHand;
            if (hand == "rock") {
                dataHand = "1";
            } else if (hand == "paper") {
                dataHand = "2";
            } else if (hand == "scissor") {
                dataHand = "3";
            }

            try {
                let dataMatch = await this.findOne({
                    where: { id: match_id },
                });

                if (!dataMatch.player_1) {
                    console.log("masuk sini ga lu?");
                    // Disini yang diupdate adalah player_1 dan player_1_hand
                    await this.update(
                        {
                            player_1: biodata_id,
                            player_1_hand: dataHand,
                        },
                        {
                            where: { id: match_id },
                        }
                    );
                } else if (!dataMatch.player_2) {
                    console.log("masuk sini ga lu kedua?");
                    // Disini yang diupdate adalah player_2 dan player_2_hand
                    let resultFight;
                    if (dataMatch.player_1_hand == dataHand) {
                        resultFight = "DRAW";
                    }
                    // Player 1 Rock
                    if (dataMatch.player_1_hand == "3" && dataHand == "1") {
                        resultFight = "PLAYER 2 WIN";
                    }
                    if (dataMatch.player_1_hand == "3" && dataHand == "2") {
                        resultFight = "PLAYER 1 WIN";
                    }
                    // Player 1 Paper
                    if (dataMatch.player_1_hand == "1" && dataHand == "3") {
                        resultFight = "PLAYER 1 WIN";
                    }
                    if (dataMatch.player_1_hand == "1" && dataHand == "2") {
                        resultFight = "PLAYER 2 WIN";
                    }
                    // Player 1 Scissors
                    if (dataMatch.player_1_hand == "2" && dataHand == "3") {
                        resultFight = "PLAYER 2 WIN";
                    }
                    if (dataMatch.player_1_hand == "2" && dataHand == "1") {
                        resultFight = "PLAYER 1 WIN";
                    }

                    await this.update(
                        {
                            player_2: biodata_id,
                            player_2_hand: dataHand,
                            result: resultFight,
                        },
                        {
                            where: { id: match_id },
                        }
                    );
                }

                let dataHasilMatch = await this.findOne({
                    where: { id: match_id },
                });

                return dataHasilMatch;
            } catch (error) {
                return Promise.resolve(error);
            }
        };

        static checkMatch = async function (match_id) {
            try {
                console.log(`match_id: ${match_id}`);
                let data = await this.findOne({
                    where: { id: match_id },
                });

                if (data) {
                    return true;
                } else {
                    return false;
                }
            } catch (error) {
                return Promise.resolve(error);
            }
        };

        static detailMatch = function (id) {
            let data = this.findOne({
                where: { id: id },
            });

            return data;
        };
    }
    matches.init(
        {
            player_1: {
                type: DataTypes.INTEGER,
                field: "player_1",
            },
            player_1_hand: DataTypes.STRING,
            player_2: {
                type: DataTypes.INTEGER,
                field: "player_2",
            },
            player_2_hand: DataTypes.STRING,
            venue_id: {
                type: DataTypes.INTEGER,
                field: "venue_id",
            },
            result: DataTypes.STRING,
            match_name: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "matches",
        }
    );
    return matches;
};
