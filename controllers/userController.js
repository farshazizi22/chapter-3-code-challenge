const { biodatas, users } = require("../models");
const usersLogin = require("../users.json");
const passport = require("passport");

const loginUser = (req, res) => {
    res.render("user/login");
};

const loginSaveUser = (req, res) => {
    // let dataUsers = usersLogin;

    // for (let i = 0; i < dataUsers.length; i++) {
    //     if (
    //         dataUsers[i]["email"] == req.body.email &&
    //         dataUsers[i]["password"] == req.body.password
    //     ) {
    //         let selectedUser = dataUsers[i];
    //         res.redirect("/dashboard");
    //     }
    // }

    // res.render("user/login", { message: "Email atau password salah" });
    // return;
};

const loginAction = passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/user/login",
    failureFlash: true,
});

const signUpUser = (req, res) => {
    res.render("user/sign-up");
};

const signUpSaveUser = async function (req, res) {
    let dataUsers = biodatas;

    // cek dengan data biodatas
    for (let i = 0; i < dataUsers.length; i++) {
        if (dataUsers[i]["username"] == req.body.username) {
            res.status(200);
            res.redirect("/user/sign-up", { message: "User telah terdaftar" });
            res.end();
            return;
        }
    }

    let dataBiodata = await biodatas.createBiodata(req.body);
    await users.register(dataBiodata, req.body.username).then(() => {
        res.redirect("/user/login");
    });
    // biodatas.create({
    //     full_name: req.body.full_name,
    //     name: req.body.name,
    //     dob: req.body.dob,
    //     username: req.body.username,
    // })
    // .then(function () {
    //     res.redirect("/user/login");
    // });
};

module.exports = {
    loginUser,
    loginSaveUser,
    loginAction,
    signUpUser,
    signUpSaveUser,
};