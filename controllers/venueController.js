const router = require("express").Router();
const { venues } = require("../models");

const listVenue = (req, res) => {
    venues.findAll().then((venues) => {
        res.render("venue/venue-list", { venues });
    });
};

const createVenue = (req, res) => {
    res.render("venue/venue-create");
};

const saveVenue = (req, res) => {
    venues
        .create({
            venue_name: req.body.venue_name,
            establishment_year: req.body.establishment_year,
            capacity: req.body.capacity,
            city: req.body.city,
            country: req.body.country,
        })
        .then(function () {
            res.redirect("/venue/list");
        });
};

const detailVenue = (req, res) => {
    venues
        .findOne({
            where: { id: req.params.id },
        })
        .then(function (venue) {
            res.render("venue/venue-detail", { venue });
        });
};

const editVenue = (req, res) => {
    venues
        .findOne({
            where: { id: req.params.id },
        })
        .then(function (venue) {
            res.render("venue/venue-edit", { venue });
        });
};

const updateVenue = (req, res) => {
    venues
        .update(
            {
                venue_name: req.body.venue_name,
                establishment_year: req.body.establishment_year,
                capacity: req.body.capacity,
                city: req.body.city,
                country: req.body.country,
            },
            {
                where: { id: req.body.id },
            }
        )
        .then(function () {
            res.redirect("/venue/list");
        });
};

const destroyVenue = (req, res) => {
    venues
        .destroy({
            where: { id: req.params.id },
        })
        .then(function () {
            res.redirect("/venue/list");
        });
};

module.exports = {
    listVenue,
    createVenue,
    saveVenue,
    detailVenue,
    editVenue,
    updateVenue,
    destroyVenue,
};