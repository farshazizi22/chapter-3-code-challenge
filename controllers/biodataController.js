const { biodatas } = require("../models");

const listBiodata = (req, res) => {
    biodatas.findAll().then((biodatas) => {
        res.render("biodata/biodata-list", { biodatas });
    });
};

const createBiodata = (req, res) => {
    res.render("biodata/biodata-create");
};

const saveBiodata = (req, res) => {
    biodatas
        .create({
            full_name: req.body.full_name,
            name: req.body.name,
            dob: req.body.dob,
            gender: parseInt(req.body.gender),
            address: req.body.address,
        })
        .then(function () {
            res.redirect("/biodata/list");
        });
};

const detailBiodata = (req, res) => {
    biodatas
        .findOne({
            where: { id: req.params.id },
        })
        .then(function (biodata) {
            res.render("biodata/biodata-detail", { biodata });
        });
};

const editBiodata = (req, res) => {
    biodatas
        .findOne({
            where: { id: req.params.id },
        })
        .then(function (biodata) {
            res.render("biodata/biodata-edit", { biodata });
        });
};

const updateBiodata = (req, res) => {
    biodatas
        .update(
            {
                full_name: req.body.full_name,
                name: req.body.name,
                dob: req.body.dob,
                gender: parseInt(req.body.gender),
                address: req.body.address,
            },
            {
                where: { id: req.body.id },
            }
        )
        .then(function () {
            res.redirect("/biodata/list");
        });
};

const destroyBiodata = (req, res) => {
    biodatas
        .destroy({
            where: { id: req.params.id },
        })
        .then(function () {
            res.redirect("/biodata/list");
        });
};

module.exports = {
    listBiodata,
    createBiodata,
    saveBiodata,
    detailBiodata,
    editBiodata,
    updateBiodata,
    destroyBiodata,
};