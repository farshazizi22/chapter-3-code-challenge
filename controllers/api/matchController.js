const { matches, users } = require("../../models");

const createMatchAction = function (req, res, next) {
    matches
        .createMatch(req.body)
        .then((match) => {
            res.json({
                id: match.id,
                match_name: match.match_name,
            });
        })
        .catch((err) => next(err));
};

const fightAction = async function (req, res, next) {
    try {
        const dataUser = await users.findOne({
            where: { id: req.user.dataValues.id },
        });
        
        const checkMatch = await matches.checkMatch(req.params.match_id);
        if (!checkMatch) {
            res.status(404);
            res.json({
                message: "Match name tidak ditemukan!",
            });
        }

        const fight = await matches.fight(
            req.params.match_id,
            req.body,
            dataUser.biodata_id
        );
        if (fight) {
            res.status(200);
            res.json({
                id: fight.id,
                player_1: fight.player_1,
                player_1_hand: fight.player_1_hand,
                player_2: fight.player_2,
                player_2_hand: fight.player_2_hand,
                result: fight.result,
                match_name: fight.match_name,
            });
        } else {
            res.status(404);
            res.json({
                message: "Fight gagal!",
            });
        }
    } catch (error) {
        next(error);
    }
};

const detailMatchAction = function (req, res, next) {
    matches
        .detailMatch(req.params.id)
        .then((match) => {
            if (match) {
                res.status(200);
                res.json(match);
            } else {
                res.status(404);
                res.json({
                    message: "Match name tidak ditemukan!",
                });
            }
        })
        .catch((err) => next(err));
};

module.exports = { createMatchAction, fightAction, detailMatchAction };
