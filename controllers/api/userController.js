const { users } = require("../../models");

const registerAction = function (req, res, next) {
    users
        .register(req.body)
        .then((user) => {
            res.json({
                id: user.id,
                username: user.username,
            });
        })
        .catch((err) => next(err));
};

const loginAction = function (req, res, next) {
    users
        .authenticate(req.body)
        .then((user) => {
            if(user) {
                res.json({
                    id: user.id,
                    username: user.username,
                    token: user.generateToken(),
                });
            } else {
                res.status(401)
                res.json({
                    message: "Role Admin tidak diizinkan!"
                });
            }
        })
        .catch((err) => next(err));
};

const profileAction = function (req, res, next) {
    res.json(req.user);
};

module.exports = { registerAction, loginAction, profileAction };
