const { matches, biodatas, venues } = require("../models/");

const listMatch = (req, res) => {
    matches
        .findAll({
            include: ["playerOne", "playerTwo"],
        })
        .then((matches) => {
            res.render("match/match-list", { matches });
        });
};

const createMatch = (req, res) => {
    biodatas.findAll().then((biodatas) => {
        venues.findAll().then((venues) => {
            res.render("match/match-create", { biodatas, venues });
        });
    });
};

const saveMatch = (req, res) => {
    let result;
    if (req.body.player_1_hand == req.body.player_2_hand) {
        result = "DRAW";
    }
    // Player 1 Rock
    if (req.body.player_1_hand == "3" && req.body.player_2_hand == "1") {
        result = "PLAYER 2 WIN";
    }
    if (req.body.player_1_hand == "3" && req.body.player_2_hand == "2") {
        result = "PLAYER 1 WIN";
    }
    // Player 1 Paper
    if (req.body.player_1_hand == "1" && req.body.player_2_hand == "3") {
        result = "PLAYER 1 WIN";
    }
    if (req.body.player_1_hand == "1" && req.body.player_2_hand == "2") {
        result = "PLAYER 2 WIN";
    }
    // Player 1 Scissors
    if (req.body.player_1_hand == "2" && req.body.player_2_hand == "3") {
        result = "PLAYER 2 WIN";
    }
    if (req.body.player_1_hand == "2" && req.body.player_2_hand == "1") {
        result = "PLAYER 1 WIN";
    }
    matches
        .create({
            player_1: req.body.player_1,
            player_1_hand: req.body.player_1_hand,
            player_2: req.body.player_2,
            player_2_hand: req.body.player_2_hand,
            result: result,
            venue_id: req.body.venue_id,
        })
        .then(function () {
            res.redirect("/match/list");
        });
};

const detailMatch = (req, res) => {
    matches
        .findOne({
            where: { id: req.params.id },
            include: ["playerOne", "playerTwo", "venue"],
        })
        .then(function (match) {
            res.render("match/match-detail", { match });
        });
};

// const detailMatch = (req, res) => {
//     matches.findOne({
//         where: { id: req.params.id },
//         include: ['playerOne', 'playerTwo']
//     })
//     .then(function (match) {
//         res.render("match/match-edit", { match });
//     });
// };

// const updateMatch = (req, res) => {
//     matches.update(
//         {
//             full_name: req.body.full_name,
//             name: req.body.name,
//             dob: req.body.dob,
//             gender: parseInt(req.body.gender),
//             address: req.body.address,
//         },
//         {
//             where: { id: req.body.id },
//         }
//     )
//     .then(function () {
//         res.redirect("/match/list");
//     });
// };

// const destroyMatch = (req, res) => {
//     matches.destroy({
//         where: { id: req.params.id },
//     })
//     .then(function () {
//         res.redirect("/match/list");
//     });
// };

module.exports = {
    listMatch,
    createMatch,
    saveMatch,
    detailMatch,
};
